const path = require("path");
const _ = require("underscore");

function parseCanvasToSimplifiedJSON(canvasObject, filePath){
  var parsedJSON = {};
  parsedJSON.networkName=path.basename(filePath, ".json");
  parsedJSON.entities=[];
  var entities = _.where(canvasObject.objects,{type:"group"});
  _.each(entities, function(entity){
//TODO: extract stereotype and split at \n
    var entityTitle = _.where(entity.objects,{type:"i-text"})[0].text;
    entityName = entityTitle.split("\n")[1];
    var stereotype = entityTitle.split("\n")[0];
    stereotype = stereotype.replace("<<","").replace(">>","");
    parsedJSON.entities.push({
      "name":entityName,
      "stereotype": stereotype
    });
    var entityAttributesArray = _.map(_.where(entity.objects,{type:"i-text"})[1].text.split("\n"), function(key){
      return {"name":key};
    });
    parsedJSON.entities.push({
      "attributes": entityAttributesArray
    });
  })
  return {
    "fileName": path.dirname(filePath) + "/" + parsedJSON.networkName + "Parsed" + ".json",
    "parsedJSON": parsedJSON
  }
}
exports.parseCanvasToSimplifiedJSON = parseCanvasToSimplifiedJSON;
