const {fabric} = require("fabric");
const {dialog} = require("electron").remote;
const read = require("read-data").sync;
const write = require("write-data").sync;
const _ = require("underscore");
const mdlConstants = require("./modelingStyles.js").mdlConstants;
const mdlParser = require("./entityModelParser");

const canvas = new fabric.Canvas(document.getElementById("canvas"));

// Double-click event handler
var fabricDblClick = function(obj, handler) {
  return function() {
    if (obj.clicked) handler(obj);
    else {
      obj.clicked = true;
      setTimeout(function() {
        obj.clicked = false;
      }, 500);
    }
  };
};

// Editing-completed event handler
var editingCompleted = function(obj, handler) {
  return function() {
    handler(obj);
  };
};

//ungroup objects on double click
var ungroup = function(group) {
  var groupItems = [];
  groupItems = group._objects;
  group._restoreObjectsState();
  canvas.remove(group);
  for (var i = 0; i < groupItems.length; i++) {
    canvas.add(groupItems[i]);
  }
  //if you have disabled rende on addition
  canvas.renderAll();
};

var getFocusedObject = function(pointer) {
  allGroups = _.where(canvas.getObjects(), {type:"group"});
  for ( var i = 0; i < allGroups.length; i++ ){
    possibleTargets = _.where(allGroups[i]._objects, {type:"i-text"});
    for ( var j = 0; j < possibleTargets.length; j++ ) {
      var bleft =  possibleTargets[i].left;
      var btop = possibleTargets[i].top;
      var bwidth = possibleTargets[i].width;
      var bheight = possibleTargets[i].height;
      if ( (bleft < pointer.x && pointer.x < (bleft + bwidth)) &&
           (btop  < pointer.y && pointer.y < (btop + bheight)) )
          {
           return possibleTarget[i];
         }
    }
  }
};


//edit focused object after ungroup
var edit = function(group) {
  ungroup(group);
  var focusObject = getFocusedObject(canvas.getPointer(event.e));
  canvas.setActiveObject(focusObject);
  focusObject.enterEditing();
  focusObject.selectAll();
};

// Editing completed
var editingDone = function(group) {
  var items = [];
  group.forEachObject(function(obj) {
    items.push(obj);
    canvas.remove(obj);
  });
  var grp = new fabric.Group(items, mdlConstants.ENTITY.GROUP_SETTINGS);
  // TODO: Update Rectangle Size!
  canvas.add(grp);
  grp.on('mousedown', fabricDblClick(grp, edit));
};

function addEntity(type) {

  var boxSettings = {};
  var sType, eName;
  if (type === "Participant") {
    boxSettings = mdlConstants.PARTICIPANT_BOX_SETTINGS;
    sType = "<<Participant>>";
    eName = "Participant Name";
  }
  if (type === "Asset") {
    boxSettings = mdlConstants.ASSET_BOX_SETTINGS;
    sType = "<<Asset>>";
    eName = "Asset Name";
  }

  // Initialize a group for the Particpant shape (to make moving easier)
  var entityGroup = new fabric.Group([], mdlConstants.ENTITY.GROUP_SETTINGS);

  // Add a surrounding Box
  var entityBox = new fabric.Rect(boxSettings);
  entityGroup.addWithUpdate(entityBox);

  //Add editable title
  var titleText = new fabric.IText(sType + "\n" + eName, mdlConstants.ENTITY.TITLE_SETTINGS);
  entityGroup.addWithUpdate(titleText);
  titleText.on("editing:exited", editingCompleted(entityGroup, editingDone));

  //Add line between title and attributes
  var titleSeparator = new fabric.Line([0, 50, 150, 50], mdlConstants.ENTITY.TITLE_SEPARATOR_SETTINGS);
  entityGroup.addWithUpdate(titleSeparator);

  // Add attributes
  var attributes = new fabric.IText("Add Attributes here...", mdlConstants.ENTITY.ATTRIBUTE_SETTINGS);
  entityGroup.addWithUpdate(attributes);
  attributes.on("editing:exited", editingCompleted(entityGroup, editingDone));

  // Make the objects of the group editable
  entityGroup.on("mousedown", fabricDblClick(entityGroup, edit));

  canvas.add(entityGroup);
}

function saveModel(){
  var savePath = dialog.showSaveDialog({
    title: "Save Model",
    defaultPath: "./projects",
    buttonLabel: "Save"
  });
  var serializedModel = canvas.toObject();
  var parsedModel = mdlParser.parseCanvasToSimplifiedJSON(serializedModel, savePath);
  write(savePath, serializedModel);
  write(parsedModel.fileName, parsedModel.parsedJSON);
  canvas.clear();
}

function loadModel(){
  canvas.clear();
  var openPath = dialog.showOpenDialog({
    title: "Load Model",
    defaultPath: "./projects",
    buttonLabel: "Load"
  });
  var serializedModel = read(openPath[0]);
  canvas.loadFromJSON(serializedModel,canvas.renderAll.bind(canvas), function (o, object){
    object.on("mousedown", fabricDblClick(object, edit));
  });
}

exports.addEntity = addEntity;
exports.saveModel = saveModel;
exports.loadModel = loadModel;
