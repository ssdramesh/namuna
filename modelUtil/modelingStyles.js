const entityWidth = 150;
const entityHeight = 200;
const entityTop = 100;
const entityLeft = 100;
const entityObjectHeight = 50;
const entityObjectVSpacing = 20;
const entityObjectHSpacing = 20;

// Constants for layout of entities
const mdlConstants = {
  ENTITY: {
    PADDING: 15,
    LINE_HEIGHT: 1,

    GROUP_SETTINGS:{
      left:entityLeft,
      top:entityTop,
      width: entityWidth,
      height: entityHeight,
      borderColor: "black",
      lockRotation: true
    },

    TITLE_SETTINGS:{
      left: entityLeft + entityObjectHSpacing,
      top: entityTop + entityObjectVSpacing,
      height: entityObjectHeight,
      textAlign: "center",
      fontFamily: "arial",
      fontSize: "14",
      lineHeight: 1,
      padding: 10,
      showITextBorder: true
    },

    TITLE_SEPARATOR_SETTINGS: {
      left: entityLeft,
      top: entityTop + entityObjectHeight + entityObjectVSpacing,
      stroke: "gray"
    },

    ATTRIBUTE_SETTINGS:{
      left: entityLeft + 0.66*entityObjectHSpacing,
      top: entityTop + 4*entityObjectVSpacing,
      textAlign: "left",
      fontFamily: "arial",
      fontSize: "14",
      lineHeight: 1,
      padding: 10,
      showITextBorder: true
    }
  },

  PARTICIPANT_BOX_SETTINGS: {
    left: entityLeft,
    top: entityTop,
    width: entityWidth,
    height: entityHeight,
    stroke: "#000",
    fill: "#aea9eb",
    borderColor: "black",
    opacity: 0.2,
    hasControls: false,
    strokeWidth: 1
  },

  ASSET_BOX_SETTINGS: {
    left: entityLeft,
    top: entityTop,
    width: entityWidth,
    height: entityHeight,
    stroke: "#000",
    fill: "#c6f6af",
    borderColor: "black",
    opacity: 0.2,
    hasControls: false,
    strokeWidth: 1
  }
}
exports.mdlConstants = mdlConstants;
