const {app, BrowserWindow, dialog} = require("electron");
// const path = require("path");
const nativeImage = require('electron').nativeImage;

let win;

var image = nativeImage.createFromPath(__dirname + '/assets/icons/png/icon_32x32@2x.png');
image.setTemplateImage(true);

function createWindow () {
  win = new BrowserWindow({
    width:1600,
    height:900,
    minWidth: 1500,
    minHeight: 800,
    title: "Namuna Modeler",
    icon: image
  });
  // win.webContents.openDevTools();
  win.loadFile("index.html");
  win.on("closed", () => {
    win = null;
  })
}

app.on("ready", createWindow);
app.on("window-all-closed", function () {
  if (process.platform !== 'darwin') {
    app.quit();
  }
})

app.on("activate", () => {
  if (win === null) {
    createWindow();
  }
})
